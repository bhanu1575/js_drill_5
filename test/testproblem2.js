 // Check the each person projects list and group the data based on the project status and return the data as below
  
  /*
  
    {
      'Completed': ["Mobile App Redesign","Product Launch Campaign","Logo Redesign","Project A".. so on],
      'Ongoing': ["Website User Testing","Brand Awareness Campaign","Website Mockup" .. so on]
      .. so on
    }
  
  */

let dataset = require('../js_drill_5.js');
const getProjectListByStatus = require('../problem2.js');

try {
    let projectListByStatus = getProjectListByStatus(dataset);
    console.log(projectListByStatus);
} catch (error) {
    console.error(error.message);
}