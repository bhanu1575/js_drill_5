// Iterate over each object in the array and in the email breakdown the email and return the output as below:
  /*
     Note: use only email property to get the below details
  
     [
        {
        firstName: 'John', // the first character should be in Uppercase
        lastName: 'Doe', // the first character should be in Uppercase
        emailDomain: 'example.com'
        },
        {
        firstName: 'Jane',
        lastName: 'Smith',
        emailDomain: 'gmail.com'
        }
        .. so on
     ]
  */
const dataset = require('../js_drill_5.js');
const getPersonDetails = require('../problem1.js');

try {
    let personDetails = getPersonDetails(dataset);
    console.log(personDetails);
} catch (error) {
    console.error(error.message);
}