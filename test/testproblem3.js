  // From the given data, filter all the unique langauges and return in an array

let dataset = require('../js_drill_5.js');
const getAllUniqueLanguages = require('../problem3.js');

try {
    let uniqueLanguages = getAllUniqueLanguages(dataset);
    console.log(uniqueLanguages);
} catch (error) {
    console.error(error.message);
}