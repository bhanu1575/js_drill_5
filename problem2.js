 // Check the each person projects list and group the data based on the project status and return the data as below
  
  /*
  
    {
      'Completed': ["Mobile App Redesign","Product Launch Campaign","Logo Redesign","Project A".. so on],
      'Ongoing': ["Website User Testing","Brand Awareness Campaign","Website Mockup" .. so on]
      .. so on
    }
  
  */

function getProjectListByStatus (dataset) {
    if(Array.isArray(dataset)) {
        let allProjectList = dataset.reduce((accumulator,person) => {
            if(person.projects) {
                accumulator.push(person.projects);
            }
            return accumulator;
        },[]).flat();
        let projectListByStatus = allProjectList.reduce((accumulator,project) => {
            if(!accumulator[project.status]) {
                accumulator[project.status] = [project.name];

            } else {
                accumulator[project.status].push(project.name);
            }
            return accumulator;
        },{});
        return projectListByStatus;
    }
}

module.exports = getProjectListByStatus;