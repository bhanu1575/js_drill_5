  // From the given data, filter all the unique langauges and return in an array

function getAllUniqueLanguages(dataset) {
    if(Array.isArray(dataset)) {
        let allLanguages = dataset.map((person) => {
            return person.languages;
        })
        .flat();
        let uniqueLanguages = allLanguages.reduce((accumulator,language) => {
            if(!accumulator.includes(language)) {
                accumulator.push(language);
            }
            return accumulator;
        },[]);
        return uniqueLanguages;

    } else {
        throw new Error('Arguments passed to the funcion getAllUniqueLanguages is not valid type');
    }
}

module.exports = getAllUniqueLanguages;