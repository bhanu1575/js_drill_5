// Iterate over each object in the array and in the email breakdown the email and return the output as below:
  /*
     Note: use only email property to get the below details
  
     [
        {
        firstName: 'John', // the first character should be in Uppercase
        lastName: 'Doe', // the first character should be in Uppercase
        emailDomain: 'example.com'
        },
        {
        firstName: 'Jane',
        lastName: 'Smith',
        emailDomain: 'gmail.com'
        }
        .. so on
     ]
  */

function getPersonDetails(dataset) {
    if(Array.isArray(dataset)) {
        let personDetails = dataset.map((person) => {
           let firstName = person.name.slice(0,person.name.indexOf(' '));
           let lastName = person.name.slice(person.name.indexOf(' ')+1);
           if(lastName==person.name) {
               firstName = lastName;
               lastName = 'LastName is not available';
           }
           let emailDomain = person.email.slice(person.email.indexOf('@')+1);
           if(emailDomain == person.email) {
            emailDomain = 'Invalid email address/emailDomain was not found';
           }
           return {'firstName': firstName, 'lastName': lastName, 'emailDomain': emailDomain};
        });
        return personDetails;
    } else {
        throw new Error('Arguments passed to the function getPersonDetails is not valid type')
    }
}
module.exports = getPersonDetails;
